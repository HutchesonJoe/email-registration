#!/usr/bin/env python3

import unittest
import email_reg
#create a test suite for email_reg.py

class TestEmailRegistration(unittest.TestCase):
    #test the get_db_connection function
    def test_get_db_connection(self):
        conn = email_reg.get_db_connection()
        self.assertIsNotNone(conn)
        conn.close()

    #test the create_table function
    def test_create_table(self):
        email_reg.create_table()
        conn = email_reg.get_db_connection()
        c = conn.cursor()
        c.execute("SELECT count(*) FROM sqlite_master WHERE type='table' AND name='subscribers'")
        result = c.fetchone()
        self.assertEqual(result[0], 1)
        conn.close()

    #test the get_subscribers function
    def test_get_subscribers(self):
        conn = email_reg.get_db_connection()
        c = conn.cursor()
        c.execute("INSERT INTO subscribers VALUES ('example@email.com')")
        conn.commit()
        subscribers = email_reg.get_subscribers()
        self.assertEqual(subscribers[0]['email'], 'example@email.com')
        #reset the database
        conn = email_reg.get_db_connection()
        c = conn.cursor()
        c.execute("DELETE FROM subscribers;")
        conn.commit()
        c.close()
        conn.close()

    #test the containerized app by checking the status code
    def test_index(self):
        tester = email_reg.app.test_client(self)
        response = tester.get('/', content_type='html/text')
        self.assertEqual(response.status_code, 200)
        
    #test the containerized app by checking the response data
    def test_index_data(self):
        tester = email_reg.app.test_client(self)
        response = tester.get('/', content_type='html/text')
        self.assertTrue(b'Email Registration' in response.data)

    #test the containerized app by checking the response data
    def test_index_post(self):
        tester = email_reg.app.test_client(self)
        response = tester.post('/', data={'email': 'example@email.com'})
        self.assertEqual(response.status_code, 302)
        self.assertEqual(response.location, '/')
        #reset the database
        conn = email_reg.get_db_connection()
        c = conn.cursor()
        c.execute("DELETE FROM subscribers;")
        conn.commit()
        c.close()
        conn.close()

    # test end-to-end functionality
    def test_end_to_end(self):
        tester = email_reg.app.test_client(self)
        response = tester.post('/', data={'email': 'example@email.com'})
        self.assertEqual(response.status_code, 302)
        self.assertEqual(response.location, '/')
        response = tester.get('/', content_type='html/text')
        self.assertTrue(b'Email Registration' in response.data)
        
    # test end-to-end functionality


if __name__ == '__main__':
    unittest.main()